import java.util.Scanner;
class Play{
	public static final int X = 1, O = -1;
	public static final int EMPTY = 0;
	
	
	public int player = X;
	private int[][] board = new int[3][3];
	public boolean isEmpty = false;
	

	public boolean isWin(int player)
	{
		return ((board[0][0] + board[0][1] + board[0][2] == player*3) ||(board[1][0] + board[1][1] + board[1][2] == player*3) ||
                (board[2][0] + board[2][1] + board[2][2] == player*3) ||(board[0][0] + board[1][0] + board[2][0] == player*3) ||
				(board[0][1] + board[1][1] + board[2][1] == player*3) ||(board[0][2] + board[1][2] + board[2][2] == player*3) ||
				(board[0][0] + board[1][1] + board[2][2] == player*3) ||(board[2][0] + board[1][1] + board[0][2] == player*3));
	}
	public void isDupicate (int x, int y) 
	{
		if(x<0 || x>2 || y<0 || y>2) {
			System.out.println("Not found position");
			return;
			
		}else if(board[x][y] != EMPTY){
			System.out.println("Dupicate position");
			return ;
		}
		
		board[x][y] = player;
		player = -player;
		
	}
	public void showResult()
	{
		if(isWin(X))
		{
			System.out.println("\n X is winner...!!");
			isEmpty=false;
		}
		else if(isWin(O))
		{
			System.out.println("\n O is winner...!!");
			isEmpty=false;
		}
		else
		{
			if(!isEmpty)
			{
				System.out.println("\n it's a Draw!!");
			}
			
		}
	}
	
	public String toString()
	{
		StringBuilder s = new StringBuilder();
		isEmpty = false;
		
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				switch(board[i][j]){
				case X: 
					s.append(" X ");
					break;
				case O: 
					s.append(" O ");
					break;
				case EMPTY: 
					s.append("   ");
					isEmpty=true;
					break;
				}
				if(j<2){
					s.append("|");
				}
					
			}
			if(i<2){
				s.append("\n-----------\n");
			}
		}
		return s.toString();
	}
	
}
public class Xo {
	public static int x =0,y=0;

	public static void main(String[] args) {
		
		Play p = new Play();
		Scanner kb = new Scanner(System.in);
		
		do{
			System.out.print(p.player==p.X?"Player X (R,C) :":"Player O (R,C) :");
			
			x=kb.nextInt();
			y=kb.nextInt();
			p.isDupicate(x,y);
			System.out.println(p.toString());
			System.out.println("_____________________________");
			p.showResult();
			
		}while(p.isEmpty);
	}
}

